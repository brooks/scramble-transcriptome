# Scramble Transcriptome

Scripts associated with the paper, "Transcriptional neighborhoods regulate transcript isoform expression"

scrambleMapper.py

scrambletools.py

isoformCluster.py
